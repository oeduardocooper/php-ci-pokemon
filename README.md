# CI API

Api em PHP com framework CodeIgniter 3, MariaDB e Docker.
Versão do Docker: Docker version 20.10.5, build 55c4c88
Versão do Docker Compose: docker-compose version 1.25.0

## Passo a passo

para executar a api

```bash
make up
```

para encerrar a api

```bash
make stop
```

ou
```bash
make kill
```

## lista de pokémons

```bash
http://localhost:8000/
```

## adminer MariaDB
###### server: mariadb
###### port: 3306
###### database: apidb
###### user: admin
###### password: nimda

```bash
http://localhost:8080
```

## executando o migration (criando o DB)

```
GET http://localhost:8000/migration
```