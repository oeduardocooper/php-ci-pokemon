<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokemon extends CI_Controller {

	private $baseUrl = 'https://pokeapi.co/api/v2/';

  	function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper(array('request'));
    }

	public function index() {

		$offset = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
		$limit = 20;

		// obtem os dados da api
        $data = json_decode(sendRequest("{$this->baseUrl}pokemon/?offset={$offset}&limit={$limit}"));

		// paginação do ci
		$this->load->library('pagination');
        $config["base_url"] = base_url() . "pokemon/index";
        $config["total_rows"] = $data->count;
        $config["per_page"] = 20;
		$config['last_link'] = 'last';
		$config['first_link'] = 'first';
		
		$this->pagination->initialize($config);
		$data->pagination = $this->pagination->create_links();
		$data->javascript = site_url('system/application/libraries/pokemon/pokemon.js');

		$this->load->view('pokemon_list', $data);
	}

	public function pokemonDetails() {
		$pokemon_id = json_decode($this->input->post('pokemonId'));

		// obtem os dados da api
        $data = json_decode(sendRequest("{$this->baseUrl}pokemon/{$pokemon_id}"));


		echo json_encode($data);
	}
}
