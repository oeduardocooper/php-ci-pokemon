$(document).ready(function (e) {
    
    $('.details').on('click', function () {

        var pokemonId = $(this).attr('id');
        
        $.ajax({
          url: base_url + 'index.php/pokemon/pokemonDetails',
          method: "post",
          data: {pokemonId: pokemonId},
          dataType: 'json',
          success: (response) => {
            $(".modal-title").text(response.name);
            $("#modalDealhes").modal('toggle');
            var divModalHtml = `
              <img src="${response.sprites.front_default}">
              <img src="${response.sprites.front_shiny}">
              <img src="${response.sprites.back_default}">
              <img src="${response.sprites.front_shiny}"><br>`;
            divModalHtml += `<label for="peso"><b>Peso:</b> ${response.weight}</label><br>`;
            divModalHtml += `<label for="altura"><b>Altura:</b> ${response.height}</label><br>`;
            var habilidades = response.abilities.map(item => item.ability.name).join(',');
            divModalHtml += `<label for="habilidade"><b>Habilidades:</b> ${habilidades}</label>`;
            var movimemtos = response.moves.map(item => item.move.name).join(',');
            divModalHtml += `<label for="habilidade"><b>Movimentos:</b> ${movimemtos}</label>`;

            $('.modal-body').html(divModalHtml); 
          },
          error: (err) => {
            alert("Ocorreu um erro: " + err);
          },
        });
    });
});