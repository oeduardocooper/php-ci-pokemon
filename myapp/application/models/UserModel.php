<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model {
 
    function getUser($username, $email){

        $this->db->select('*');
        $this->db->from('user');
        
        if($username) {
            $this->db->where('username', $username);
        }
        if($email) {
            $this->db->where('email', $email);
        }
        
        $query = $this->db->get();
        return $query->result();
    }
 
}
?>