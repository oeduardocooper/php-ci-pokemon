<?php


class AuthHelper {

    public static function validateToken($headers){
        $ci =& get_instance();
        
        $ci->load->helper('jwt');

        if(!isset($headers['Authorization'])){
            return null;
        }

        $token = $headers['Authorization'];
        $jwt = new JWT();

        $payload = $jwt->decode($token, JWT_KEY);
        return $payload;
    }

}