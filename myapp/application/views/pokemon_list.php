<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Lista de Pokémons</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	p.pagination {
		text-align: center;
		font-size: 22px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}	

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	.back{
    	background: url('http://i652.photobucket.com/albums/uu246/yxyshopping/---------------Fake%20Hair/AAAAA5A/----------AAAAAAAA-----------/Pokemon_Banner.jpg') 0 0 repeat-x;
    	position:relative;
    	padding:148px; 10px;
    	background-size: cover;
		height: 297px;
	}
	.titulo {
		font-size: 4.5rem;
		color: whitesmoke;
		font-weight: bold;
	}
	</style>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>application/libraries/pokemon/pokemon.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= base_url() ?>application/libraries/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>application/libraries/css/bootstrap.min.css">

</head>
<body>

<div id="container" class="col-lg-12">
	<div id="body">
		<div class="col-lg-12 back">
			<h1 class="titulo">Lista de Pokemóns</h1>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-hover table-bordered">
					<tr>
						<td><strong>#</strong></td>
						<td><strong>Nome</strong></td>
						<td><strong>Ações</strong></td>
					</tr>
					<?php if(is_array($results) && count($results) ) {
						foreach($results as $pokemon){
							$pokemon_id = explode('/', $pokemon->url)[6];
					?>
					<tr>
						<td><?php echo $pokemon_id; ?></td>
						<td><?php echo $pokemon->name; ?></td>
						<td><button type="button" class="btn btn-default details" id="<?php echo $pokemon_id; ?>" >Detalhes</button></td>
						
					</tr>
					<?php } 
					}?>
				</table>
			</div>
		</div>
	</div>
	<p class="pagination"><?php echo $pagination; ?> Total: <b><?php echo $count; ?></b></p>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>

<div class="modal fade"id="modalDealhes" role="dialog">
<div class="modal-dialog">
  
  <div class="modal-content">
    <div class="modal-header">

     <h4 class="modal-title">Pokémon Name</h4>
    </div>
    <div class="modal-body Content">
		<!-- conteúdo dinâmico -->						
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
  </div>

</div>