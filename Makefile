ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(ARGS):;@:)

MAKEFLAGS += --silent

up:
	docker-compose up -d

stop:
	docker stop $$(docker ps -a -q) && docker rm $$(docker ps -a -q) 
	
kill: 
	docker-compose kill -s SIGINT
